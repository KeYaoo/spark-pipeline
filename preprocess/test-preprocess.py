import argparse 
import csv
import os
import shutil
import sys
import time
import boto3

from mleap import pyspark
from mleap.pyspark.spark_support import SimpleSparkSerializer

from pyspark.sql import SparkSession
from pyspark.ml import Pipeline, Transformer, Estimator
from pyspark.ml.feature import (
    OneHotEncoderEstimator,
    StringIndexer,
    VectorAssembler,
    VectorIndexer,
)

from pyspark.sql.types import (
    LongType,
    DecimalType,
    TimestampType,
    IntegerType,
    BooleanType,
    DoubleType,
    StringType,
    StructField,
    StructType,
)

from pyspark import keyword_only  
from pyspark.ml.util import DefaultParamsReadable, DefaultParamsWritable 
from pyspark.ml.param.shared import *
from pyspark.sql.functions import *



def main():
    parser = argparse.ArgumentParser(description="app inputs and outputs")
    parser.add_argument("--s3_input_bucket", type=str, help="s3 input bucket")
    parser.add_argument("--s3_input_key_prefix", type=str, help="s3 input key prefix")
    parser.add_argument("--s3_output_bucket", type=str, help="s3 output bucket")
    parser.add_argument("--s3_output_key_prefix", type=str, help="s3 output key prefix")
    parser.add_argument("--s3_model_bucket", type=str, help="s3 model bucket")
    parser.add_argument("--s3_model_key_prefix", type=str, help="s3 model key prefix")
    args = parser.parse_args()

    spark = SparkSession.builder.appName("PySparkApp").getOrCreate()

    #This is needed to save RDDs which is the only way to write nested Dataframes into CSV format
    spark.sparkContext._jsc.hadoopConfiguration().set("mapred.output.committer.class",
                                                     "org.apache.hadoop.mapred.FileOutputCommitter")
    
    

    from pyspark.ml.feature import VectorAssembler, StandardScaler, OneHotEncoder, StringIndexer
    from pyspark.ml import Pipeline, PipelineModel
    from pyspark.sql import Row
    
    schema = StructType([StructField("name", StringType(), True), 
                         StructField("age", IntegerType(), True)])

    l = [('Alice', 1), ('Bob', 2)]
    rdd = spark.sparkContext.parallelize(l)
    Person = Row('name', 'age')
    person = rdd.map(lambda r: Person(*r))
    df2 = spark.createDataFrame(person, schema)
    
    dataset_imputed = df2
    
    name_indexer = StringIndexer(inputCol='name', outputCol='indexed_name')
    name_encoder = OneHotEncoderEstimator(inputCols=['indexed_name'], outputCols=['name_one_hot'])
    feature_assembler = VectorAssembler(inputCols=['name_one_hot', "age"], outputCol="features")
    feature_pipeline = [name_indexer, name_encoder, feature_assembler]
    featurePipeline = Pipeline(stages=feature_pipeline)
    sparkFeaturePipelineModel = featurePipeline.fit(dataset_imputed)

    sparkFeaturePipelineModel.serializeToBundle("jar:file:/tmp/model.zip", sparkFeaturePipelineModel.transform(dataset_imputed))
    #SimpleSparkSerializer().serializeToBundle(feature_pipeline, "jar:file:/tmp/model.zip", sparkTransformed)
    # Serialize the tokenizer via MLeap and upload to S3
    #pipeline.serializeToBundle("jar:file:/tmp/model.zip", transformed_df)

    # Unzip as SageMaker expects a .tar.gz file but MLeap produces a .zip file.
    import zipfile
    with zipfile.ZipFile("/tmp/model.zip") as zf:
        zf.extractall("/tmp/model")

    # Write back the content as a .tar.gz file
    import tarfile
    with tarfile.open("/tmp/model.tar.gz", "w:gz") as tar:
        tar.add("/tmp/model/bundle.json", arcname='bundle.json')
        tar.add("/tmp/model/root", arcname='root')

    s3 = boto3.resource('s3')
    file_name = os.path.join(args.s3_model_key_prefix, 'model.tar.gz')
    s3.Bucket(args.s3_model_bucket).upload_file('/tmp/model.tar.gz', file_name)
    
    
if __name__ == "__main__":
    main()
