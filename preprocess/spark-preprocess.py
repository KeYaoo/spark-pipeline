import argparse 
import csv
import os
import shutil
import sys
import time
import boto3

from mleap import pyspark
from mleap.pyspark.spark_support import SimpleSparkSerializer

from pyspark.sql import SparkSession
from pyspark.ml import Pipeline, Transformer, Estimator
from pyspark.ml.feature import (
    OneHotEncoderEstimator,
    StringIndexer,
    VectorAssembler,
    VectorIndexer,
)

from pyspark.sql.types import (
    LongType,
    DecimalType,
    TimestampType,
    IntegerType,
    BooleanType,
    DoubleType,
    StringType,
    StructField,
    StructType,
)

from pyspark import keyword_only  
from pyspark.ml.util import DefaultParamsReadable, DefaultParamsWritable 
from pyspark.ml.param.shared import *
from pyspark.sql.functions import *

from mleap.pyspark.spark_support import SimpleSparkSerializer


class DateTimeEstimator(Estimator, HasInputCols, 
        HasOutputCols, DefaultParamsReadable, DefaultParamsWritable):

    @keyword_only
    def __init__(self, inputCols=None, outputCols=None):
        super(DateTimeEstimator, self).__init__()
        kwargs = self._input_kwargs
        self.setParams(**kwargs)

    # Required in Spark >= 3.0
    def setInputCols(self, value):
        """
        Sets the value of :py:attr:`inputCols`.
        """
        return self._set(inputCols=value)

    # Required in Spark >= 3.0
    def setOutputCols(self, value):
        """
        Sets the value of :py:attr:`outputCols`.
        """
        return self._set(outputCols=value)

    @keyword_only
    def setParams(self, inputCols=None, outputCols=None):
        kwargs = self._input_kwargs
        return self._set(**kwargs)

    def _fit(self, df):
        input_cols = self.getInputCols()
        return DateTimeTransformer(input_cols)
        

class DateTimeTransformer(Transformer):
    def __init__(self, datetime_feature):
        super(DateTimeTransformer, self).__init__()
        self.datetime_feature = datetime_feature
    
    def _transform(self, df):
        feature_cols = []
        for col in self.datetime_feature:
            df = df.withColumn("{col}_hour".format(col=col), hour(col))
            df = df.withColumn("{col}_day_of_week".format(col=col), dayofweek(col))
            df = df.withColumn("{col}_day_of_month".format(col=col), dayofmonth(col))
            df = df.withColumn("{col}_day_of_year".format(col=col), dayofyear(col))
            df = df.withColumn("{col}_month".format(col=col), month(col))

        return df

def csv_line(data):
    r = ','.join(str(d) for d in data[1])
    return str(data[0]) + "," + r

def main():
    parser = argparse.ArgumentParser(description="app inputs and outputs")
    parser.add_argument("--s3_input_bucket", type=str, help="s3 input bucket")
    parser.add_argument("--s3_input_key_prefix", type=str, help="s3 input key prefix")
    parser.add_argument("--s3_output_bucket", type=str, help="s3 output bucket")
    parser.add_argument("--s3_output_key_prefix", type=str, help="s3 output key prefix")
    parser.add_argument("--s3_model_bucket", type=str, help="s3 model bucket")
    parser.add_argument("--s3_model_key_prefix", type=str, help="s3 model key prefix")
    args = parser.parse_args()

    spark = SparkSession.builder.appName("PySparkApp").getOrCreate()

    #This is needed to save RDDs which is the only way to write nested Dataframes into CSV format
    spark.sparkContext._jsc.hadoopConfiguration().set("mapred.output.committer.class",
                                                     "org.apache.hadoop.mapred.FileOutputCommitter")
    
    # Defining the schema corresponding to the input data. Only required for csv input. 
    schema = StructType([StructField("offer_id", LongType(), True), 
                         StructField("origin", StringType(), True),
                         StructField("dest", StringType(), True),
                         StructField("carrier", StringType(), True),
                         StructField("base_offer_amount", DecimalType(), True),
                         StructField("currency", StringType(), True),
                         StructField("locale", StringType(), True), 
                         StructField("departure_time", TimestampType(), True), 
                         StructField("departure_time_server", TimestampType(), True),
                         StructField("duration", IntegerType(), True),
                         StructField("fare_class", StringType(), True),
                         StructField("flight_number", IntegerType(), True),
                         StructField("instant_upgrade", BooleanType(), True),
                         StructField("is_recent_offer", BooleanType(), True),
                         StructField("offer_amount", DecimalType(), True),
                         StructField("offer_amount_total", DecimalType(), True), 
                         StructField("offer_expiration_cutoff_local", TimestampType(), True), 
                         StructField("offer_expiration_cutoff_server", TimestampType(), True),
                         StructField("offer_expiration_minutes", IntegerType(), True),
                         StructField("offer_modification_cutoff_hours", IntegerType(), True),
                         StructField("offer_modification_cutoff_local", TimestampType(), True),
                         StructField("offer_modification_cutoff_server", TimestampType(), True),
                         StructField("pax", IntegerType(), True), 
                         StructField("point_of_sale", StringType(), True), 
                         StructField("product_id", LongType(), True),
                         StructField("quantity", IntegerType(), True),
                         StructField("refundable", BooleanType(), True),
                         StructField("slider_lower_limit", DecimalType(), True),
                         StructField("slider_starting_amount", DecimalType(), True),
                         StructField("slider_upper_limit", DecimalType(), True),
                         StructField("status", StringType(), True), 
                         StructField("submission_date", TimestampType(), True), 
                         StructField("upgrade_type", StringType(), True),
                         StructField("has_errors", BooleanType(), True),
                         StructField("offer_amount_usd", DecimalType(), True),
                         StructField("offer_amount_total_usd", DecimalType(), True),
                         StructField("slider_min_usd", DecimalType(), True), 
                         StructField("slider_max_usd", DecimalType(), True), 
                         StructField("slider_starting_usd", DecimalType(), True),
                         StructField("equip", StringType(), True),
                         StructField("distance", IntegerType(), True)
                         ])
    
    df = spark.read.csv(('s3://' + os.path.join(args.s3_input_bucket, 'data/machine_learning_table.csv')), 
                                                   header=True, schema=schema)
    
    #Filter the ticketed offers
    #df = df.filter(df.status == "TICKETED")
    
#     feature_cols = ["origin", "dest", "carrier", "flight_number", "currency", "fare_class", "point_of_sale", "upgrade_type", "equip", \
#                     "departure_time", "departure_time_server", "offer_expiration_cutoff_local", "offer_modification_cutoff_local", \
#                     "quantity", "pax", "distance", "offer_amount_total_usd"]
    
#     #Select relevant feature columns
#     df = df.select(feature_cols)

    #Filling columns has null values with mean
    col_avg = df.agg( { "offer_amount_total_usd": "mean" } ).head()[0] 
    col_avg = float(col_avg)
    df = df.fillna({'offer_amount_total_usd': col_avg})


    #Calculate number of offers per flight per departure_time. group by status 
    df2 = df.groupBy("flight_number", "carrier", "departure_time").sum("quantity") \
                                                    .withColumnRenamed("sum(quantity)", "label")
    df = df2.join(df, on=["flight_number", "carrier", "departure_time"], how="inner")
    
    
    #Categorical indexer on the columns have categorical value
    #equip column is not useful, consider drop it in the SQL query
    categorical_feature = ["origin", "dest", "carrier", "currency", "fare_class",
                    "point_of_sale", "upgrade_type"]

    categorical_cols = []
    for col in categorical_feature:
        categorical_col = "categorical_" + col
        categorical_cols.append(categorical_col)

    categorical_feature_indexers = [StringIndexer(inputCol=x, outputCol="categorical_{}".format(x)) for x in categorical_feature]

    #indexer = StringIndexer(inputCols=categorical_feature, outputCols=categorical_cols)


    #One-hot-encoder for indexed columns
    one_hot_cols = []
    for col in categorical_cols:
        one_hot_cols.append("one_hot{}".format(col.lstrip("categorical")))

    onehotencoder = OneHotEncoderEstimator(inputCols=categorical_cols, outputCols=one_hot_cols)


    #Datetime paser to transform timestamps features to day of week, month of year, etc. 
    datetime_feature = ["departure_time"]

    datetime_cols = []
    for col in datetime_feature:
        df = df.withColumn("{col}_hour".format(col=col), hour(col))
        df = df.withColumn("{col}_day_of_week".format(col=col), dayofweek(col))
        df = df.withColumn("{col}_day_of_month".format(col=col), dayofmonth(col))
        df = df.withColumn("{col}_day_of_year".format(col=col), dayofyear(col))
        df = df.withColumn("{col}_month".format(col=col), month(col))
        
        datetime_cols.append("{col}_hour".format(col=col))
        datetime_cols.append("{col}_day_of_week".format(col=col))
        datetime_cols.append("{col}_day_of_month".format(col=col))
        datetime_cols.append("{col}_day_of_year".format(col=col))
        datetime_cols.append("{col}_month".format(col=col))

    #datetime_estimator = DateTimeEstimator(inputCols=datetime_feature, outputCols=datetime_cols)

    training_cols = categorical_cols + one_hot_cols + datetime_cols + ["quantity", "pax", "offer_amount_total_usd"]

    assembler = VectorAssembler(
        inputCols=training_cols,
        outputCol="features")
    
    pipeline_stages = categorical_feature_indexers + [onehotencoder, assembler]

    #The pipeline comprises of the steps added above
    pipeline = Pipeline(stages=pipeline_stages)

    #This step trains the feature transformers and transforms the dataset with information obtained from the fit.
    model = pipeline.fit(df)
    transformed_df = model.transform(df)

    
    #Keep features and label columns only
    selected_df = transformed_df.select("features", "label")

    (train_df, validation_df) = selected_df.randomSplit([0.8, 0.2])
    
    #Save the train dataframe in csv format and upload to S3
    train_rdd = train_df.rdd.map(lambda x: (x.label, x.features))
    train_lines = train_rdd.map(csv_line)
    train_lines.saveAsTextFile('s3://' + os.path.join(args.s3_output_bucket, args.s3_output_key_prefix, 'train'))
    
    #Savethe validation dataframe in csv format and upload to S3
    validation_rdd = validation_df.rdd.map(lambda x: (x.label, x.features))
    validation_lines = validation_rdd.map(csv_line)
    validation_lines.saveAsTextFile('s3://' + os.path.join(args.s3_output_bucket, args.s3_output_key_prefix, 'validation'))
    
    # Serialize the tokenizer via MLeap and upload to S3
    model.serializeToBundle("jar:file:/tmp/model.zip", model.transform(df))

    # Unzip as SageMaker expects a .tar.gz file but MLeap produces a .zip file.
    import zipfile
    with zipfile.ZipFile("/tmp/model.zip") as zf:
        zf.extractall("/tmp/model")

    # Write back the content as a .tar.gz file
    import tarfile
    with tarfile.open("/tmp/model.tar.gz", "w:gz") as tar:
        tar.add("/tmp/model/bundle.json", arcname='bundle.json')
        tar.add("/tmp/model/root", arcname='root')

    s3 = boto3.resource('s3')
    file_name = os.path.join(args.s3_model_key_prefix, 'model.tar.gz')
    s3.Bucket(args.s3_model_bucket).upload_file('/tmp/model.tar.gz', file_name)
    
    
if __name__ == "__main__":
    main()
